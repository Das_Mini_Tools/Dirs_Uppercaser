﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dirs_Uppercaser
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
                if (args[0].Contains("version"))
                {
                    Console.WriteLine(Application.ProductName + " " + Application.ProductVersion + " by " + Application.CompanyName);
                    Console.ReadKey(true);
                    Environment.Exit(0);
                }

            string[] dirs = Directory.GetDirectories(".", "*", SearchOption.AllDirectories);

            if (dirs.Length == 0)
            {
                Console.WriteLine("No directories found.");
                Console.ReadKey(true);
                Environment.Exit(0);
            }

            foreach (string dir in dirs)
            {
                Directory.Move(dir, dir + "_");
                Directory.Move(dir + "_", dir.ToUpper());
            }
        }
    }
}
